;;; Packages --- Configuration packages
;;; Commentary:
;;; Code:

(require 'use-package)

;;; Use packages
(use-package color-theme-sanityinc-tomorrow
  :ensure t
  :init
  (load-theme 'sanityinc-tomorrow-night t))

(use-package company
  :ensure t
  :init
  (global-company-mode 1)
  (setq company-idle-delay 0.3))

(use-package nlinum
  :ensure t
  :init
  (global-linum-mode))

(use-package powerline
  :ensure t
  :init
  (powerline-default-theme))

(use-package eldoc-mode
  :hook (emacs-lisp-mode
         lisp-interaction-mode
         ielm-mode
         cider-mode
         clojure-mode
         cider-repl-mode
         elixir-mode))

(use-package yasnippet
  :ensure t
  :init
  (yas-global-mode 1))

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(use-package projectile
  :ensure t
  :init
  (projectile-mode 1)
  :bind-keymap
  ("C-c p" . projectile-command-map))

(use-package smartparens
  :ensure t
  :bind (("C-c s <right>" . sp-forward-slurp-sexp)
         ("C-c s <left>" . sp-forward-barf-sexp))
  :init
  (setq sp-show-pair-from-inside nil)
  (require 'smartparens-config)
  (smartparens-global-mode)
  :diminish smartparens-mode)

(use-package counsel
  :bind
  (("\C-s" . swiper)
   ("C-c C-r" . ivy-resume)
   ("<f6>" . ivy-resume)
   ("M-x" . counsel-M-x)
   ("C-x C-f" . counsel-find-file)
   ("<f1> f" . counsel-describe-function)
   ("<f1> v" . counsel-describe-variable)
   ("<f1> o" . counsel-describe-symbol)
   ("<f1> l" . counsel-find-library)
   ("<f2> i" . counsel-info-lookup-symbol)
   ("<f2> u" . counsel-unicode-char)
   ("C-c g" . counsel-git)
   ("C-c j" . counsel-git-grep)
   ("C-c k" . counsel-ag)
   ("C-x l" . counsel-locate)
   ("C-S-o" . counsel-rhythmbox)
   :map
   minibuffer-local-map ("C-r" . counsel-minibuffer-history))
  :config
  (progn
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t)
    (setq ivy-count-format "(%d/%d) ")))

(use-package counsel-projectile
  :ensure t)

(use-package counsel-jq
  :ensure t)

(use-package ag
  :ensure t)

(use-package magit
  :ensure t)

;; (use-package rainbow-delimiters
;;   :ensure t
;;   :hook (prog-mode))

(use-package tree-sitter-langs
  :ensure t
  :init
  (global-tree-sitter-mode))

(use-package lsp-mode
  :ensure t
  :commands lsp
  :diminish lsp-mode
  :bind-keymap
  ("C-c C-l" . lsp-command-map)
  :init
  (setq lsp-zig-zls-executable "~/zls/zls")
  (add-to-list 'exec-path "~/src/elixir-ls/release")
  :config
  (setq lsp-file-watch-threshold 200000)
  (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]\\.elixir_ls\\'")
  (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]\\deps\\'")
  :hook
  ((zig-mode . lsp)
   (elixir-mode . lsp)
   (php-mode . lsp)
   (python-mode . lsp)))

(use-package lsp-ivy
  :ensure t)

(use-package lsp-ui
  :ensure t
  :commands (lsp-ui-mode))


(use-package zig-mode
  :ensure t)

(use-package cider
  :ensure t)

(use-package flycheck-clj-kondo
  :ensure t)

(use-package clojure-mode
  :ensure t
  :config
  (require 'flycheck-clj-kondo))

(use-package ob-php
  :ensure t)

(use-package org
  :after (ein)
  :ensure t
  :hook '((org-babel-after-execute . org-redisplay-inline-images))
  :init
  (global-set-key (kbd "C-c l") #'org-store-link)
  (global-set-key (kbd "C-c a") #'org-agenda)
  (global-set-key (kbd "C-c c") #'org-capture)
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((dot . t)
     (emacs-lisp . t)
     (sql . t)
     (jq . t)
     (shell . t)
     (restclient . t)
     (php . t)
     )))

(use-package ob-restclient :ensure t)
(use-package ob-async :ensure t)
(use-package ob-clojurescript :ensure t)
(use-package ob-graphql :ensure t)
(use-package ob-elixir :ensure t)
(use-package ox-gfm :ensure t)
(use-package htmlize :ensure t)
(use-package jq-mode :ensure t)
;;; elixir

(use-package flycheck-credo
  :ensure t
  :config
  (require 'lsp-mode)
  (defun custom-flycheck-credo-after-lsp ()
    (add-hook 'lsp-after-initialize-hook
              (lambda ()
                (if (equal major-mode 'elixir-mode)
                    (flycheck-add-next-checker 'lsp 'elixir-credo)))))
  :init
  (flycheck-credo-setup)
  :hook
  '((elixir-mode . custom-flycheck-credo-after-lsp)))

(use-package elixir-yasnippets
  :ensure t)

;; nice but not enough
(use-package inf-elixir
  :ensure t
  :bind (("C-c i i" . 'inf-elixir)
         ("C-c i p" . 'inf-elixir-project)
         ("C-c i l" . 'inf-elixir-send-line)
         ("C-c i r" . 'inf-elixir-send-region)
         ("C-c i b" . 'inf-elixir-send-buffer)))


(use-package alchemist
  :ensure t
  :hook (elixir-mode . alchemist-mode))

(use-package mix
  :ensure t
  :hook '((elixir-mode . mix-minor-mode)))

(use-package elixir-mode
  :ensure t
  :config
  (require 'lsp-mode)
  (defun custom-elixir-format-before-save ()
    (add-hook 'before-save-hook #'lsp-format-buffer))
  :hook
  '((elixir-mode . custom-elixir-format-before-save)))

;; php
(use-package php-mode :ensure t)
(use-package php-eldoc :ensure t)
(use-package flycheck-phpstan :ensure t)
(use-package twig-mode :ensure t)
(use-package php-cs-fixer :ensure t)

;; yaml
(use-package yaml-mode :ensure t)
(use-package flycheck-yamllint :ensure t)

;; markdown
(use-package markdown-mode :ensure t)
(use-package markdown-preview-mode :ensure t)

;; toml
(use-package toml-mode :ensure t)

;; python

(use-package python-mode :ensure t)

(use-package poetry :ensure t)

(use-package python-isort
  :ensure t
  :hook (python-mode . python-isort-on-save-mode))

;; (use-package pyenv-mode
;;   :ensure t
;;   :config
;;   (defun custom-projectile-pyenv-mode-set ()
;;     "Set pyenv version matching project name."
;;     (let ((project (projectile-project-name)))
;;       (if (member project (pyenv-mode-versions))
;;           (pyenv-mode-set project)
;;         (pyenv-mode-unset))))
;;   :init
;;   (pyenv-mode)
;;   :hook (python-mode . custom-projectile-pyenv-mode-set))

(use-package pyenv-mode
  :ensure t
  :init (pyenv-mode))

(use-package python-docstring
  :ensure t)

(use-package ein
  :ensure t
  :init
  (setq ein:output-area-inlined-images t)
  (require 'ob-ein))

;; slack
(use-package slack
  :ensure t
  ;; :commands (slack-start)
  :init
  (setq slack-buffer-emojify t)
  (setq slack-prefer-current-team t)
  :config
  (slack-register-team
   :name "prima"
   :default t
   :token "xoxc-2166649775-71523295235-1437127028341-3a50abfbf03f00f007128712b61beb2769cd8b70a23a9c8bfd9be33125054779"
   :cookie "FihazHn1CCBzAvQc3R%2FBE22mkPrQ5l96Xk3D0CP5IOz%2BNmZ25YXjzQE%2BzjHnAjmfFk1XUEvdFt2N9oIFFvN4AZ8FrUhcyICZs0RhYXsDi8JeUdfog5RYlHpkHlgGVUpWmb9DVyzSzly9vdMvZnI7x1h4ZHoUpqABJ2wa62RG00Vx5oRaOz8O8w%3D%3D"
   :subscribed-channels '(team-engine team-engine-private)
   :full-and-display-names t))

(use-package emojify
  :ensure t)

(use-package alert
  :ensure t
  :commands (alert)
  :init
  (setq alert-default-style 'notifier))

;;; docker

(use-package dockerfile-mode
  :ensure t)

(use-package docker-compose-mode
  :ensure t)

(use-package docker-tramp
  :ensure t)

(use-package counsel-tramp
  :ensure t)


;;; treemacs for python only

(use-package treemacs-all-the-icons
  :ensure t)

(use-package lsp-treemacs
  :ensure t)


(use-package treemacs-projectile
  :ensure t)

(use-package treemacs-magit
  :ensure t)

;; wabi
(when (file-exists-p "~/gits/wabi/emacs/wabi-mode.el")
  (load "~/gits/wabi/emacs/wabi-mode.el"))

;;; config.el ends here
