;;; Packages --- UI stuff cleanup
;;; Commentary:
;;; Code:

(setq initial-scratch-message ""
      inhibit-startup-message t
      tab-width 2)

(setq-default indent-tabs-mode nil)

(add-hook 'before-save-hook 'delete-trailing-whitespace)
(add-hook 'after-init-hook (lambda () (set-frame-parameter nil 'fullscreen 'fullboth)))
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(set-fringe-mode '(8 . 0))
(display-time-mode 1)
(put 'erase-buffer 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)


;;; clean.el ends here
