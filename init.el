;;; packages --- Initialize emacs
;;; Commentary:
;;; Code:
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)

(load "~/.emacs.d/packages.el")
(load "~/.emacs.d/clean.el")
(load "~/.emacs.d/config.el")

;;; init.el ends here
