

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(cljr-inject-dependencies-at-jack-in t)
 '(custom-enabled-themes '(sanityinc-tomorrow-bright))
 '(custom-safe-themes
   '("e6df46d5085fde0ad56a46ef69ebb388193080cc9819e2d6024c9c6e27388ba9" "c7eb06356fd16a1f552cfc40d900fe7326ae17ae7578f0ef5ba1edd4fdd09e58" "bf798e9e8ff00d4bf2512597f36e5a135ce48e477ce88a0764cfb5d8104e8163" "c2e1201bb538b68c0c1fdcf31771de3360263bd0e497d9ca8b7a32d5019f2fae" "1b8d67b43ff1723960eb5e0cba512a2c7a2ad544ddb2533a90101fd1852b426e" "c82d24bfba431e8104219bfd8e90d47f1ad6b80a504a7900cbee002a8f04392f" "e11569fd7e31321a33358ee4b232c2d3cf05caccd90f896e1df6cab228191109" "06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" default))
 '(display-time-mode t)
 '(elm-format-on-save t)
 '(enable-local-variables :all)
 '(ensime-eldoc-hints 'T)
 '(flycheck-after-syntax-check-hook nil)
 '(flycheck-check-syntax-automatically '(save mode-enabled))
 '(flycheck-clojure-inject-dependencies-at-jack-in t)
 '(flycheck-disabled-checkers '(elixir-dogma))
 '(flycheck-elixir-credo-strict t)
 '(inferior-lisp-program "sbcl" t)
 '(lsp-file-watch-threshold 50000)
 '(lsp-headerline-breadcrumb-enable nil)
 '(lsp-pylsp-plugins-flake8-enabled nil)
 '(lsp-ui-doc-enable t)
 '(lsp-ui-doc-use-childframe t)
 '(lsp-ui-flycheck-enable t)
 '(lsp-ui-peek-enable t)
 '(lsp-ui-sideline-enable t)
 '(org-agenda-files '("~/ownCloud/todo.org"))
 '(package-directory-list '("/usr/share/emacs/site-lisp/elpa" "~/.emacs.d/extra"))
 '(package-selected-packages
   '(ob-php jq-mode format-all ein treemacs-magit treemacs-projectile treemacs-all-the-icons counsel-tramp counser-tramp docker-tramp pyenv-mode python-docstring ox-gfm ob-elixir ob-graphql ob-clojurescript ob-async ob-restclient flycheck-clj-kondo counsel-jq projectile flycheck yasnippet toml-mode python-isort markdown-live-preview-mode htmlize ob-shell inf-elixir tree-sitter-indent tree-sitter-langs memento memoize pyenv ivy-posframe poetry python-mode typescript-mode lsp-ui use-package quelpa quelpa-use-package with-editor with-emacs julia-snail zig-mode dired-view-data ess-smart-equals ess-smart-underscore ess-view ess-view-data flycheck-julia julia-mode julia-repl julia-shell julia-vterm lsp-julia ob-ess-julia ob-julia-vterm company-flx 0blayout flx-ido ejc-sql csv-mode indent-guide ox-asciidoc ox-html5slide ox-slack calfw-org ivy-omni-org org org-ac org-alert org-gnome org-super-agenda org-tag-beautify py-autopep8 magit company-posframe posframe doneburn-theme ample-theme ample-zen-theme xml-format json-mode magit-gh-pulls magit-lfs mix elixir-mix flycheck-yamllint dumb-diff dumb-jump all-the-icons all-the-icons-dired all-the-icons-gnus all-the-icons-ibuffer all-the-icons-ivy-rich adoc-mode company-erlang erlang ac-alchemist vterm vterm-toggle redis lsp-treemacs ccls elixir-lsp php-cs-fixer elcord nyan-mode format-sql sqlup-mode exsqlaim-mode verb lsp-ivy go-projectile flymake-golangci go-eldoc go-gen-test go-guru go-mode flycheck-plantuml plantuml-mode rust-auto-mode ob-rust flycheck-rust smtpmail-multi mu4e-maildirs-extension mu4e-query-fragments mu4e-overview mu4e-jump-to-list mu4e-conversation mu4e-alert js2-mode elixir-mode markdown-mode yaml-mode flymake-cursor php-auto-yasnippets company-php cargo racer rust-auto-use rust-mode rust-playground flycheck-tip flycheck-purescript psc-ide psci purescript-mode ac-php flycheck-ensime ensime dired-sidebar dired-single undo-tree slack graphql-mode restclient company composer flycheck-phpstan phpstan php-boris php-mode cider clojure-mode nlinum counsel-projectile swiper counsel ivy helm-core helm lsp-mode srefactor zenburn-theme web-beautify twig-mode smartparens slime restclient-helm ranger rainbow-delimiters powerline popwin phpunit php-scratch php-refactor-mode php-extras php-eldoc php-completion php-boris-minor-mode phan nlinum-relative nlinum-hl markdown-preview-mode magit-filenotify js2-refactor ibuffer-projectile ibuffer-git helm-clojuredocs helm-cider-history helm-cider flycheck-pos-tip flycheck-mix flycheck-elm flycheck-dialyxir flycheck-credo flycheck-color-mode-line flycheck-clojure elm-yasnippets elm-mode elixir-yasnippets edit-indirect dockerfile-mode docker-compose-mode company-restclient color-theme-sanityinc-tomorrow cljr-helm c-eldoc alchemist ag))
 '(php-cs-fixer-command "/home/francesco/gits/prima/bin/php-cs-fixer")
 '(scheme-program-name "scheme")
 '(sql-connection-alist
   '(("local_hal9000"
      (sql-product 'postgres)
      (sql-user "hal9000")
      (sql-password "hal9000")
      (sql-server "localhost")
      (sql-database "hal9000")
      (sql-port 15432))
     ("qa_hal9000"
      (sql-product 'postgres)
      (sql-user "hal9000")
      (sql-password "hal9000")
      (sql-server "10.43.22.120")
      (sql-database "hal9000"))
     ("qa_prima"
      (sql-product 'mysql)
      (sql-user "root")
      (sql-password "prima")
      (sql-server "10.43.22.120")
      (sql-database "prima"))
     ("prod_postgres"
      (sql-product 'postgres)
      (sql-user "fbracchi")
      (sql-server "prima-aurora-production-pgsql.cluster-cc9r5ky64fkl.eu-west-1.rds.amazonaws.com")
      (sql-database "hal9000"))
     ("prod_prima"
      (sql-product 'mysql)
      (sql-user "prima_ro2")
      (sql-password "eeP4odahmie7WahThei5eeR3l")
      (sql-server "db-prima-aurora-production-read-1.prima.it")
      (sql-database "prima"))
     ("redshift"
      (sql-product 'postgres)
      (sql-user "prima")
      (sql-password "q3SbzpsQxNMuDL6Rwyr3pGjY")
      (sql-server "prima-bi-01.cfrlbxomg4hj.eu-west-1.redshift.amazonaws.com")
      (sql-database "prima"))
     ("**prima rw**"
      (sql-product 'mysql)
      (sql-user "fbracchi")
      (sql-password "DGQ5jKLdzEzEzkHAquf4hqH5")
      (sql-server "db-prima-aurora-production.prima.it")
      (sql-database "prima"))))
 '(tool-bar-mode nil)
 '(warning-suppress-types '((comp))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(nlinum ((t (:background "#2a2a2a" :foreground "#666" :underline nil :slant normal)))))
